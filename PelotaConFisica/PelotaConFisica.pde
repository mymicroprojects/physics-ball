boolean warp = true;

float window_width = 680;
float window_height = 730;

long time_initial = 0;
long time_update = 0;

long time_delay = 10;
float k_launch = 3;

float mouseX_prev = 0;
float mouseY_prev = 0;
float mouseX_speed = 0;
float mouseY_speed = 0;

float dragging = 0;

float k = 1000/0.7; //  Constant gravity multipler
float km = 100;

float k_floor_friction = 10;

//  Ellipse coordinates and size
float ball[] = {
  50,    // x         0
  50,    // y         1
  70     // diameter  2
};
float x = 50;
float y = 50;
float s = 70;

// Floor coordinates
float floor_x1 = 0;
float floor_y1 = 730;
float floor_x2 = 680;
float floor_y2 = 600;
float floor_angle = 0;
float floor_friction = 0.1 * k_floor_friction;

float blocks[][] = {{
0     // x1        0
,730  // y1        1
,680  // x2        2
,600  // y2        3
,0    // angle     4
,1    // Friction  5
},
{
0     // x1        0
,730  // y1        1
,680  // x2        2
,600  // y2        3
,0    // angle     4
,1    // Friction  5
}
};

//  Ellipse speed and direction
float xspeed = 0;
float yspeed = 0;
float xdir = 1;
float ydir = 1;

float stopping = 0;

// Ellipse elasticity (more elesticity, more bounce)
float elasticity = 0.8;

// Ellipse's mass
float mass = 0;

//  Mouse position when it clicks
float mx = 0;
float my = 0;

//  Constant of gravity and current gravity
float gravity = 9.8 / k;
float g = gravity;

float drag_aux = 0;

void setup(){
  size(680,730);
  frameRate(60);
  smooth();
  //  Draw a rect by oposite corners
  rectMode(CORNERS);
}

void draw(){
  background(180,230,255);
  
  draw_shapes();
  prfloat_data();  
  physics();
  warp();

  time_initial = millis();
  
  if(dragging == 1){
    dragging();
  }
  else{
    not_dragging();
  }
}

void mouseDragged(){
  //  If the mouse drags and its left button is pressed and it is in the shape
  if (mouseButton == LEFT && intheshape() == 1){
    dragging = 1;
  }
}

void mousePressed(){
  //  Set the click coordinates
  mx = mouseX;
  my = mouseY;
  //  Check if the click was made on the ball's area
  if(mouseButton == LEFT && intheshape() == 1){
    //  Do not let the ball be affected by other forces than the mouse drag movement
    //  while it is been dragged
    g = 0;
    yspeed = 0;
    xspeed = 0;
    if(collision(x-s/2,y-s/2,x+s/2,y+s/2,blocks[0][0],blocks[0][1],blocks[0][2],blocks[0][3]) == 1){
      drag_aux = 1;
    }
  }
  
  if(mouseButton == RIGHT){
    if(intheshape() == 1){
      s += 5;
    }else{
      s -= 5;
    }
  }
}

void mouseReleased(){
  //  Turn on the gravity again
  g = gravity;
  //  Set last speed by difference of mouse positions
  dragging = 0;
}

float intheshape(){
  //  Check if the mouse click initial position was made in the ball's area
  if((mx >= x - s && mx <= x + s)&&(my >= y - s && my <= y + s)){
    // Return 1 if so
    return 1;
  }
  else{
    return 0;
  }
}

float collision(float x3,float y3, float x4, float edge1, float x1, float y1,float x2,float edge2){
  //  Check if the bottom part of the ball and the top part of the floor collisions
  //  by comparing their coordinates
  if(edge1 >= edge2){
    //  Set the ball's position above the floor in order not to let the ball
    //  pass trough the floor
    y = floor_y2 - s/2;
    return 1;
  }
  else{
    return 0;
  }
}

void draw_shapes(){
  fill(255);
  ellipse(x,y,s,s);
  
  fill(0,255,0);
  rect(floor_x1,floor_y1,floor_x2,floor_y2);
}

void prfloat_data(){
  //  Prfloat  mouse coordinates
  fill(255,0,0);
  textSize(20);
  text("Mouse X", 20, 20);
  text("Mouse Y", 120, 20);
  text(mouseX, 20, 40);
  text(mouseY, 120, 40);
  
  //  Prfloat  ball's propieties
  fill(50,50,255);
  textSize(20);
  text("Ball X", 220, 20);
  text("Ball Y", 320, 20);
  text(x, 220, 40);
  text(y, 320, 40);
  fill(230,110,110);
  text("Ball speed in y: ", 420, 20);
  text(yspeed, 580,20);
  fill(110,150,230);
  text("Ball speed in x: ", 420, 40);
  text(xspeed, 580,40);
  fill(255,255,0);
  text("Elasticity: ", 20, 70);
  text(elasticity, 120,70);
  fill(255,0,255);
  text("Gravity: ", 220, 70);
  text(gravity * k, 320,70);
  fill(180,0,180);
  text("Mass: ", 420, 70);  
  text(mass, 520,70);
  fill(0,150,0);
  text("Potential Energy: ", 20, 100);  
  text(-1 * (gravity * mass * (y - (floor_y2 - s/2))) , 200,100);
}

void physics(){
  mass = (PI * (s/2) * (s/2)) / km;
  stopping = floor_friction * gravity;
  
  if(collision(x-s/2,y-s/2,x+s/2,y+s/2,floor_x1,floor_y1,floor_x2,floor_y2) == 1 && (yspeed == -1 * gravity || yspeed == 0)){
    yspeed = 0;
  }else{
    //  Add gravity aceleration to current speed in the correct direction
    yspeed -= g * mass;
    y -= yspeed * ydir;
  }
  
  x += xspeed * xdir;
  
  //  Check if floor and ball collision
  if(collision(x-s/2,y-s/2,x+s/2,y+s/2,floor_x1,floor_y1,floor_x2,floor_y2) == 1){
    bounce();
  }
  
  friction();
}

void mouseSpeed(){
  if(time_initial > time_update + time_delay){
    
    mouseY_speed = (mouseY - mouseY_prev)/k_launch;
    mouseX_speed = abs((mouseX - mouseX_prev)/k_launch);
    
    mouseX_prev = mouseX;
    mouseY_prev = mouseY;
    time_update = millis();    
  }
  if(pmouseX > mouseX){
    xdir = -1;
  }else if(pmouseX < mouseX){
    xdir = 1;
  }
}

void keyPressed(){
  if(key == '+'){
    x = 200;
    y = 100;
    yspeed = 0;
    xspeed = 0;
  }
}

void dragging(){
  if(collision(x-s/2,y-s/2,x+s/2,y+s/2,floor_x1,floor_y1,floor_x2,floor_y2) == 1 && drag_aux == 0){
    bounce();
    dragging = 0;
    g = gravity;
  }
  if(drag_aux == 1 && y < floor_y2){
    drag_aux = 0;
  }
    mouseSpeed();
    //  Set ball's coordinates relative to mous's click position
    x = mouseX + (x - mx);
    y = mouseY + (y - my);
    //  Set the initial mouse click position to the current mouse position
    mx = mouseX;
    my = mouseY;
}

void not_dragging(){
    yspeed += -1 * mouseY_speed;
    xspeed += mouseX_speed;
    mouseY_speed = 0;
    mouseX_speed = 0;
}

void bounce(){
      //  If the speed is lower than 1, stop bouncing at all
      if(abs(yspeed) < 1){
        yspeed = 0;
        y = floor_y2 - s/2;
      }      
    //  Make the ball bounce by inverting its speed multiplied by its elasticity
    yspeed *= -1 * elasticity;
}

void friction(){
  if(collision(x-s/2,y-s/2,x+s/2,y+s/2,floor_x1,floor_y1,floor_x2,floor_y2) == 1 && drag_aux == 0){
    if((xspeed >= 0 - stopping && xspeed <= 0 + stopping)||xspeed == 0){
      xspeed = 0;
    }else{
      xspeed -= stopping;
    }   
  }
}

void warp(){
  if(warp == true){
    if(x - s > window_width){
      x = 0 - s;
    }
    if(x + s < 0){
      x = window_width + s;
    } 
  }
}
