# Description
A basic physics simulation of a ball from scratch. Made in Processing.
It can be dragged, can be thrown, can bounce, etc.

Showcase video (spanish): [https://youtu.be/z7nMUCoQzN0](https://youtu.be/z7nMUCoQzN0)

# Images
![phyisicsBall_screen1.jpg](media/phyisicsBall_screen1.jpg)
